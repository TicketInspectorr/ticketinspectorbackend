package dao;

import entity.BusLineEntity;
import entity.BusStopsLinesEntity;

import java.util.Collection;

/**
 * Created by braders on 10.03.2017.
 */
public interface BusStopLinesRepository extends GenericRepository<Integer, BusStopsLinesEntity> {

    Collection<BusStopsLinesEntity> getAllBusStopLines(int id);
}
