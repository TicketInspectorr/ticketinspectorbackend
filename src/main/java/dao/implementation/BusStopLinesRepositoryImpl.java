package dao.implementation;

import dao.BusStopLinesRepository;
import entity.BusStopsLinesEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.UtilsDatabase;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static utils.Const.UNSUPPORTED_OPERATION_CASE;

/**
 * Created by braders on 10.03.2017.
 */

@Component
public class BusStopLinesRepositoryImpl implements BusStopLinesRepository {

    @Autowired
    private UtilsDatabase utilsDatabase;

    @Override
    public Optional<BusStopsLinesEntity> get(Integer integer) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }

    @Override
    public Collection<BusStopsLinesEntity> getAll() {
        Function<Session, Collection<BusStopsLinesEntity>> function = session -> session.createQuery("from BusStopsLinesEntity").list();
        Collection<BusStopsLinesEntity> collection = utilsDatabase.exceuteQuery(function);
        return collection;
    }

    @Override
    public boolean insert(BusStopsLinesEntity busStopsHasBusLineEntity) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }

    @Override
    public boolean delete(Integer integer) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }

    @Override
    public boolean deleteAll() {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }

    @Override
    public Collection<BusStopsLinesEntity> getAllBusStopLines(int idBusStop) {

        Function<Session, Collection<BusStopsLinesEntity>> function = session -> {
            Query query = session.createQuery("from BusStopsLinesEntity e where e.fkBusStations = :idBusStop");
            query.setParameter("idBusStop", idBusStop);
            return query.list();
        };

        Collection<BusStopsLinesEntity> collection = utilsDatabase.exceuteQuery(function);
        return collection;
    }
}
