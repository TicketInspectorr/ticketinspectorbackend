package dao;

import entity.BusStopEntity;

/**
 * Created by braders on 10.03.2017.
 */
public interface BusStopRepository extends GenericRepository <Integer, BusStopEntity> {
}
