package dao;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by braders on 10.03.2017.
 */
public interface GenericRepository <ID, T> {

    Optional<T> get(ID id);

    Collection<T> getAll();

    boolean insert(T t);

    boolean delete(ID id);

    boolean deleteAll();

}
