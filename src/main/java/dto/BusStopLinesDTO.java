package dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by braders on 10.03.2017.
 */
@Builder
@Getter
@Setter
public class BusStopLinesDTO {
    private String line;
    private boolean cechkingTicket;
}
