package dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by braders on 10.03.2017.
 */
@Setter
@Getter
@Builder
public class BusStopDTO implements Serializable {
    private int idBusStop;
    private String nameBusStop;
    private double latitude;
    private double longitude;

    @Override
    public boolean equals(Object o) {
        BusStopDTO that = (BusStopDTO) o;
        return nameBusStop == that.nameBusStop;

    }

    @Override
    public int hashCode() {
        return idBusStop;
    }

}
