package routes;

import bean.process.GetBusesStopsLinesBean;
import bean.process.GetNearestBusStopBean;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static utils.Const.RESLET_METHODS_GET;
import static utils.Const.URL;

/**
 * Created by braders on 14.04.2017.
 */
@Component
public class NearestBusStopRoute extends RouteBuilder {

    private static final String RESOURCE_NEAREST_BUS_STOP = "/nearestBusStop";

    @Override
    public void configure() throws Exception {

        from(URL + RESOURCE_NEAREST_BUS_STOP + RESLET_METHODS_GET).bean(GetNearestBusStopBean.class).transform().body();

    }
}
