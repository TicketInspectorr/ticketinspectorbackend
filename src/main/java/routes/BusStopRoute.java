package routes;

import bean.process.GetBusStopLinesBean;
import bean.process.GetBusesStopsLinesBean;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static utils.Const.RESLET_METHODS_GET;
import static utils.Const.RESOURCE_ID;
import static utils.Const.URL;

/**
 * Created by Braders-Laptop on 2017-03-07.
 */
@Component
public class BusStopRoute extends RouteBuilder {

    private static final String RESOURCE_BUS_STOP = "/busStop";

    @Override
    public void configure() throws Exception {

        from(URL + RESOURCE_BUS_STOP + RESLET_METHODS_GET).bean(GetBusesStopsLinesBean.class).transform().body();

        from(URL + RESOURCE_BUS_STOP + RESOURCE_ID + RESLET_METHODS_GET).bean(GetBusStopLinesBean.class).transform().body();
    }
}
