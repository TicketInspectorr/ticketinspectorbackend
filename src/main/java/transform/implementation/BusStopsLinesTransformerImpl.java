package transform.implementation;

import dto.BusStopLinesDTO;
import entity.BusStopsLinesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import transform.BusStopTransformer;
import transform.BusStopsLinesTransformer;

import static utils.Const.UNSUPPORTED_OPERATION_CASE;

/**
 * Created by braders on 10.03.2017.
 */
@Component
public class BusStopsLinesTransformerImpl implements BusStopsLinesTransformer {

    @Autowired
    BusStopTransformer busStopTransformer;

    @Override
    public BusStopLinesDTO converToDto(BusStopsLinesEntity busStopsLinesEntity) {
        return BusStopLinesDTO.builder().line(busStopsLinesEntity.getFkBusLine())
                .cechkingTicket(busStopsLinesEntity.getCheckingTicket()).build();
    }

    @Override
    public BusStopsLinesEntity convertToEntity(BusStopLinesDTO busStopLinesDTO) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }
}
