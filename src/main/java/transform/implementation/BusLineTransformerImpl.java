package transform.implementation;

import dto.BusLineDTO;
import entity.BusLineEntity;
import entity.BusStopsLinesEntity;
import org.springframework.stereotype.Component;
import transform.BusLineTransformer;

import static utils.Const.UNSUPPORTED_OPERATION_CASE;

/**
 * Created by braders on 10.03.2017.
 */
@Component
public class BusLineTransformerImpl implements BusLineTransformer {

    @Override
    public BusLineDTO converToDto(BusLineEntity busLineEntity) {
        return BusLineDTO.builder()
                .line(busLineEntity.getBusLineId())
                .build();
    }

    @Override
    public BusLineEntity convertToEntity(BusLineDTO busLineDTO) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }

    @Override
    public BusLineDTO convertToDto(BusStopsLinesEntity busStopsLinesEntity) {
        return BusLineDTO.builder()
                .line(busStopsLinesEntity.getFkBusLine())
                .checkingTicket(busStopsLinesEntity.getCheckingTicket())
                .build();
    }
}
