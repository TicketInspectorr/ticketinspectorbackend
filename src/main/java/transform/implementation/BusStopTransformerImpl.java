package transform.implementation;

import dto.BusStopDTO;
import entity.BusStopEntity;
import org.springframework.stereotype.Component;
import transform.BusStopTransformer;

import static utils.Const.UNSUPPORTED_OPERATION_CASE;

/**
 * Created by braders on 10.03.2017.
 */
@Component
public class BusStopTransformerImpl implements BusStopTransformer {

    @Override
    public BusStopDTO converToDto(BusStopEntity busStationsEntity) {
        return BusStopDTO.builder().idBusStop(busStationsEntity.getBusStopId())
                .nameBusStop(busStationsEntity.getName())
                .latitude(busStationsEntity.getLatitude())
                .longitude(busStationsEntity.getLongitude())
                .build();
    }

    @Override
    public BusStopEntity convertToEntity(BusStopDTO busStopDTO) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_CASE);
    }

}
