package transform;

import dto.BusStopDTO;
import entity.BusStopEntity;

/**
 * Created by braders on 10.03.2017.
 */
public interface BusStopTransformer extends GenericTransformer<BusStopDTO, BusStopEntity> {
}
