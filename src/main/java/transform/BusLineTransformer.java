package transform;

import dto.BusLineDTO;
import entity.BusLineEntity;
import entity.BusStopsLinesEntity;

/**
 * Created by braders on 10.03.2017.
 */
public interface BusLineTransformer extends GenericTransformer<BusLineDTO, BusLineEntity> {
    BusLineDTO convertToDto(BusStopsLinesEntity busStopsLinesEntity);
}
