package transform;

/**
 * Created by braders on 10.03.2017.
 */
public interface GenericTransformer<DTO, ENTITY> {

    DTO converToDto(ENTITY entity);

    ENTITY convertToEntity(DTO dto);
}
