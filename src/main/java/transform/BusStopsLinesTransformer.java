package transform;

import dto.BusStopLinesDTO;
import entity.BusStopsLinesEntity;

/**
 * Created by braders on 10.03.2017.
 */
public interface BusStopsLinesTransformer extends GenericTransformer<BusStopLinesDTO, BusStopsLinesEntity> {
}
