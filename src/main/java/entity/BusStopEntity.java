package entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by braders on 10.03.2017.
 */
@Entity
@Table(name = "bus_stop", schema = "ticketinspector", catalog = "")
public class BusStopEntity {
    private int busStopId;
    private String name;
    private double latitude;
    private double longitude;
    private Collection<BusStopsLinesEntity> busStopsLinesByBusStopId;

    @Id
    @Column(name = "bus_stop_id", nullable = false)
    public int getBusStopId() {
        return busStopId;
    }

    public void setBusStopId(int busStopId) {
        this.busStopId = busStopId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "latitude", nullable = false, precision = 0)
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude", nullable = false, precision = 0)
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BusStopEntity that = (BusStopEntity) o;

        if (busStopId != that.busStopId)
            return false;
        if (Double.compare(that.latitude, latitude) != 0)
            return false;
        if (Double.compare(that.longitude, longitude) != 0)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = busStopId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "busStopByFkBusStations")
    public Collection<BusStopsLinesEntity> getBusStopsLinesByBusStopId() {
        return busStopsLinesByBusStopId;
    }

    public void setBusStopsLinesByBusStopId(Collection<BusStopsLinesEntity> busStopsLinesByBusStopId) {
        this.busStopsLinesByBusStopId = busStopsLinesByBusStopId;
    }
}
