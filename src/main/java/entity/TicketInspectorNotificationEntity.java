package entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by braders on 10.03.2017.
 */
@Entity
@Table(name = "ticket_inspector_notification", schema = "ticketinspector", catalog = "")
@IdClass(TicketInspectorNotificationEntityPK.class)
public class TicketInspectorNotificationEntity {
    private int ticketInspectorNotificationId;
    private Date date;
    private Integer duration;
    private int fkBusStopsLines;

    @Id
    @Column(name = "ticket_inspector_notification_id", nullable = false)
    public int getTicketInspectorNotificationId() {
        return ticketInspectorNotificationId;
    }

    public void setTicketInspectorNotificationId(int ticketInspectorNotificationId) {
        this.ticketInspectorNotificationId = ticketInspectorNotificationId;
    }

    @Basic
    @Column(name = "date", nullable = true)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "duration", nullable = true)
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Id
    @Column(name = "fk_bus_stops_lines", nullable = false)
    public int getFkBusStopsLines() {
        return fkBusStopsLines;
    }

    public void setFkBusStopsLines(int fkBusStopsLines) {
        this.fkBusStopsLines = fkBusStopsLines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        TicketInspectorNotificationEntity that = (TicketInspectorNotificationEntity) o;

        if (ticketInspectorNotificationId != that.ticketInspectorNotificationId)
            return false;
        if (fkBusStopsLines != that.fkBusStopsLines)
            return false;
        if (date != null ? !date.equals(that.date) : that.date != null)
            return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ticketInspectorNotificationId;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + fkBusStopsLines;
        return result;
    }
}
