package entity;

import javax.persistence.*;

/**
 * Created by braders on 10.03.2017.
 */
@Entity
@Table(name = "bus_stops_lines", schema = "ticketinspector", catalog = "")
@IdClass(BusStopsLinesEntityPK.class)
public class BusStopsLinesEntity {
    private int busStopsLinesId;
    private int fkBusStations;
    private String fkBusLine;
    private Boolean checkingTicket;
    private BusLineEntity busLineByFkBusLine;
    private BusStopEntity busStopByFkBusStations;

    @Id
    @Column(name = "bus_stops_lines_id", nullable = false)
    public int getBusStopsLinesId() {
        return busStopsLinesId;
    }

    public void setBusStopsLinesId(int busStopsLinesId) {
        this.busStopsLinesId = busStopsLinesId;
    }

    @Id
    @Column(name = "fk_bus_stations", nullable = false)
    public int getFkBusStations() {
        return fkBusStations;
    }

    public void setFkBusStations(int fkBusStations) {
        this.fkBusStations = fkBusStations;
    }

    @Id
    @Column(name = "fk_bus_line", nullable = false, length = 4)
    public String getFkBusLine() {
        return fkBusLine;
    }

    public void setFkBusLine(String fkBusLine) {
        this.fkBusLine = fkBusLine;
    }

    @Basic
    @Column(name = "checking_ticket", nullable = true)
    public Boolean getCheckingTicket() {
        return checkingTicket;
    }

    public void setCheckingTicket(Boolean checkingTicket) {
        this.checkingTicket = checkingTicket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BusStopsLinesEntity that = (BusStopsLinesEntity) o;

        if (busStopsLinesId != that.busStopsLinesId)
            return false;
        if (fkBusStations != that.fkBusStations)
            return false;
        if (fkBusLine != null ? !fkBusLine.equals(that.fkBusLine) : that.fkBusLine != null)
            return false;
        if (checkingTicket != null ? !checkingTicket.equals(that.checkingTicket) : that.checkingTicket != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = busStopsLinesId;
        result = 31 * result + fkBusStations;
        result = 31 * result + (fkBusLine != null ? fkBusLine.hashCode() : 0);
        result = 31 * result + (checkingTicket != null ? checkingTicket.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "fk_bus_line", referencedColumnName = "bus_line_id", nullable = false, insertable = false, updatable = false)
    public BusLineEntity getBusLineByFkBusLine() {
        return busLineByFkBusLine;
    }

    public void setBusLineByFkBusLine(BusLineEntity busLineByFkBusLine) {
        this.busLineByFkBusLine = busLineByFkBusLine;
    }

    @ManyToOne
    @JoinColumn(name = "fk_bus_stations", referencedColumnName = "bus_stop_id", nullable = false, insertable = false, updatable = false)
    public BusStopEntity getBusStopByFkBusStations() {
        return busStopByFkBusStations;
    }

    public void setBusStopByFkBusStations(BusStopEntity busStopByFkBusStations) {
        this.busStopByFkBusStations = busStopByFkBusStations;
    }
}
