package entity;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by braders on 10.03.2017.
 */
@Entity
@Table(name = "bus_line", schema = "ticketinspector", catalog = "")
public class BusLineEntity {
    private String busLineId;
    private Collection<BusStopsLinesEntity> busStopsLinesByBusLineId;

    @Id
    @Column(name = "bus_line_id", nullable = false, length = 4)
    public String getBusLineId() {
        return busLineId;
    }

    public void setBusLineId(String busLineId) {
        this.busLineId = busLineId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BusLineEntity that = (BusLineEntity) o;

        if (busLineId != null ? !busLineId.equals(that.busLineId) : that.busLineId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return busLineId != null ? busLineId.hashCode() : 0;
    }

    @OneToMany(mappedBy = "busLineByFkBusLine")
    public Collection<BusStopsLinesEntity> getBusStopsLinesByBusLineId() {
        return busStopsLinesByBusLineId;
    }

    public void setBusStopsLinesByBusLineId(Collection<BusStopsLinesEntity> busStopsLinesByBusLineId) {
        this.busStopsLinesByBusLineId = busStopsLinesByBusLineId;
    }
}
