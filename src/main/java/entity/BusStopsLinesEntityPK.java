package entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by braders on 10.03.2017.
 */
public class BusStopsLinesEntityPK implements Serializable {
    private int busStopsLinesId;
    private int fkBusStations;
    private String fkBusLine;

    @Column(name = "bus_stops_lines_id", nullable = false)
    @Id
    public int getBusStopsLinesId() {
        return busStopsLinesId;
    }

    public void setBusStopsLinesId(int busStopsLinesId) {
        this.busStopsLinesId = busStopsLinesId;
    }

    @Column(name = "fk_bus_stations", nullable = false)
    @Id
    public int getFkBusStations() {
        return fkBusStations;
    }

    public void setFkBusStations(int fkBusStations) {
        this.fkBusStations = fkBusStations;
    }

    @Column(name = "fk_bus_line", nullable = false, length = 4)
    @Id
    public String getFkBusLine() {
        return fkBusLine;
    }

    public void setFkBusLine(String fkBusLine) {
        this.fkBusLine = fkBusLine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BusStopsLinesEntityPK that = (BusStopsLinesEntityPK) o;

        if (busStopsLinesId != that.busStopsLinesId)
            return false;
        if (fkBusStations != that.fkBusStations)
            return false;
        if (fkBusLine != null ? !fkBusLine.equals(that.fkBusLine) : that.fkBusLine != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = busStopsLinesId;
        result = 31 * result + fkBusStations;
        result = 31 * result + (fkBusLine != null ? fkBusLine.hashCode() : 0);
        return result;
    }
}
