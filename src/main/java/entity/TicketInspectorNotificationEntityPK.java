package entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by braders on 10.03.2017.
 */
public class TicketInspectorNotificationEntityPK implements Serializable {
    private int ticketInspectorNotificationId;
    private int fkBusStopsLines;

    @Column(name = "ticket_inspector_notification_id", nullable = false)
    @Id
    public int getTicketInspectorNotificationId() {
        return ticketInspectorNotificationId;
    }

    public void setTicketInspectorNotificationId(int ticketInspectorNotificationId) {
        this.ticketInspectorNotificationId = ticketInspectorNotificationId;
    }

    @Column(name = "fk_bus_stops_lines", nullable = false)
    @Id
    public int getFkBusStopsLines() {
        return fkBusStopsLines;
    }

    public void setFkBusStopsLines(int fkBusStopsLines) {
        this.fkBusStopsLines = fkBusStopsLines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        TicketInspectorNotificationEntityPK that = (TicketInspectorNotificationEntityPK) o;

        if (ticketInspectorNotificationId != that.ticketInspectorNotificationId)
            return false;
        if (fkBusStopsLines != that.fkBusStopsLines)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ticketInspectorNotificationId;
        result = 31 * result + fkBusStopsLines;
        return result;
    }
}
