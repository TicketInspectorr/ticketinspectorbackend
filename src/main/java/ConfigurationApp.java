import org.apache.camel.CamelContext;
import org.apache.camel.spi.PackageScanClassResolver;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.camel.spring.javaconfig.Main;
import org.apache.commons.dbcp.BasicDataSource;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import routes.BusStopRoute;
import routes.NearestBusStopRoute;
import utils.UtilsDatabase;

/**
 * Created by Braders-Laptop on 2017-03-07.
 */
@Configuration
@ComponentScan(basePackages = {"dao", "transform"}, basePackageClasses = {BusStopRoute.class, UtilsDatabase.class})
public class ConfigurationApp extends CamelConfiguration {

    @Autowired
    private BusStopRoute busStopRoute;

    @Autowired
    private NearestBusStopRoute nearestBusStopRoute;

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.setConfigClass(ConfigurationApp.class);
        main.run();
    }

    @Override
    protected void setupCamelContext(CamelContext camelContext) throws Exception {
        camelContext.addRoutes(busStopRoute);
        camelContext.addRoutes(nearestBusStopRoute);
    }

    @Bean
    BasicDataSource dataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/ticketinspector");
        basicDataSource.setUsername("root");
        return basicDataSource;
    }

    @Bean
    LocalSessionFactoryBean localSessionFactoryBean() {
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(dataSource());
        localSessionFactoryBean.setPackagesToScan(new String[]{"entity"});
        return localSessionFactoryBean;
    }
}
