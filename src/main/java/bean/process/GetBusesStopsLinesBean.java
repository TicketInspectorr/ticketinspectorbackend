package bean.process;

import bean.AbstractRestfullBeanProcessor;
import dao.BusStopLinesRepository;
import dto.BusStopDTO;
import dto.BusStopLinesDTO;
import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import transform.BusLineTransformer;
import transform.BusStopTransformer;
import transform.BusStopsLinesTransformer;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

/**
 * Created by braders on 10.03.2017.
 */

public class GetBusesStopsLinesBean extends AbstractRestfullBeanProcessor {

    @Autowired
    BusStopLinesRepository busStopLinesRepository;

    @Autowired
    BusStopsLinesTransformer busStopsLinesTransformer;

    @Autowired
    BusStopTransformer busStopTransformer;


    @Override
    public void process(Exchange exchange) throws Exception {

        Map<BusStopDTO, List<BusStopLinesDTO>> busStopLinesResponse = busStopLinesRepository.getAll().stream()
                .collect(groupingBy(busStopsLinesEntity -> busStopTransformer
                                .converToDto(busStopsLinesEntity.getBusStopByFkBusStations()),
                        mapping(busStopsLinesEntity -> busStopsLinesTransformer.
                                converToDto(busStopsLinesEntity), toList())));

        exchange.getOut().setBody(convertToJson(busStopLinesResponse));
    }
}
