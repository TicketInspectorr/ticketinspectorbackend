package bean.process;

import bean.AbstractRestfullBeanProcessor;
import com.google.common.base.Preconditions;
import dao.BusStopLinesRepository;
import dto.BusLineDTO;
import entity.BusStopsLinesEntity;
import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import transform.BusLineTransformer;
import utils.Precodition;

import java.util.Collection;
import java.util.stream.Collectors;

import static utils.Const.HEADER_ELEMENT_ID;

/**
 * Created by braders on 30.03.2017.
 */
public class GetBusStopLinesBean extends AbstractRestfullBeanProcessor {

    @Autowired
    BusStopLinesRepository busStopLinesRepository;

    @Autowired
    BusLineTransformer busLineTransformer;

    @Override
    public void process(Exchange exchange) throws Exception {

        String idBusStop = exchange.getIn().getHeader(HEADER_ELEMENT_ID, String.class);

        Preconditions.checkArgument(Precodition.isInteger(idBusStop), "Invalid user ID passed to argument: " + idBusStop);

        Collection<BusStopsLinesEntity> busLineEntities = busStopLinesRepository.getAllBusStopLines(Integer.parseInt(idBusStop));

        Collection<BusLineDTO> busLinesResponse = busLineEntities.stream()
                .map(busLineTransformer::convertToDto)
                .collect(Collectors.toList());

        exchange.getOut().setBody(convertToJson(busLinesResponse));
    }
}
