package bean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.camel.Processor;

import java.lang.reflect.Type;

/**
 * Created by braders on 10.03.2017.
 */
public abstract class AbstractRestfullBeanProcessor implements Processor {
    private Gson gson;

    public AbstractRestfullBeanProcessor() {
        this.gson = new GsonBuilder().enableComplexMapKeySerialization().create();
    }

    protected String convertToJson(Object object) {
        return gson.toJson(object);
    }

    protected <T> T convertFromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }
}
