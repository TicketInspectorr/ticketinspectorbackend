package utils;

/**
 * Created by Braders-Laptop on 2017-03-07.
 */
public class Const {
   public final static String URL = "restlet:http://localhost:8080";

   public final static String RESLET_METHODS_GET = "?restletMethods=get";

   public static final String RESOURCE_ID = "/{id}";
   public static final String HEADER_ELEMENT_ID = "id";

   public final static String UNSUPPORTED_OPERATION_CASE = "No supported operation yet.";
}
