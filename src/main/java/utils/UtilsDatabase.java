package utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Created by braders on 10.03.2017.
 */
@Component
public class UtilsDatabase {

    @Autowired
    private SessionFactory sessionFactory;

    public <T> T exceuteQuery(Function<Session, T> consumer) {
        Session session = sessionFactory.openSession();
        T result = consumer.apply(session);
        session.close();
        return result;
    }
}
